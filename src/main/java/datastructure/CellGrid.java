package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private CellState[][] board;
    
	
	public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
		this.cols = columns;
		board = new CellState[rows][cols];
		for ( int i=0; i<rows; i++) {
			for (int j=0; j<cols; j++) {
				board[i][j]= initialState;
			}
		}
		 
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

//    @Override
//    public void set(int row, int column, CellState element) throws IndexOutOfBoundsException {
//        if (row>=0 && row<numRows()) {
//        	if (column>=0 && column<numColumns()) {
//        		board[row][column] = element;
//        		
//        	}
//        } 
//        else {
//        	throw new IndexOutOfBoundsException("This location is not on the board");
//        }
//        
//    }
    @Override
    public void set(int row, int column, CellState element)  {
    	board[row][column] = element;
    }

//    @Override
//    public CellState get(int row, int column) throws IndexOutOfBoundsException {
//    	CellState atLocation = null;
//    	if (row>=0 && row<numRows()) {
//        	if (column>=0 && column<numColumns()) {
//        		atLocation = board[row][column];
//        	}
//    	}else {
//    		throw new IndexOutOfBoundsException("This location is not on the board");
//    	}
//    	
//        return atLocation;
//    }
    
    @Override
    public CellState get(int row, int column){
    	CellState atLocation = null;
        atLocation = board[row][column];
        return atLocation;
    }    

    @Override
    public IGrid copy() {
    	int cRows = this.rows;
    	int cCols = this.cols;
    	IGrid boardCopy = new CellGrid(cRows, cCols, CellState.DEAD);
    	for (int r = 0; r < numRows(); r++) {
    		for (int c = 0; c < numColumns(); c++) {
    			boardCopy.set(r, c , this.get(r, c));
    		}
    	}
    	return boardCopy;
    }
}
