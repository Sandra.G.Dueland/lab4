package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		CellState current = null;
		current = currentGeneration.get(row, col);
		return current;
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int r = 0; r < numberOfRows(); r++) {
			for (int c = 0; c < numberOfColumns(); c++) {
				CellState nextState = this.getNextCell(r, c);
				nextGeneration.set(r, c, nextState);	
			}	
		}
		currentGeneration = nextGeneration;
	}

	
	public CellState getNextCell(int row, int col) {
		CellState nextState = null;
		CellState currentCS = getCellState(row,col);
		int aliveNeighbours = countNeighbors(row,col,CellState.ALIVE);
		
		//if the current cell is alive
		if (currentCS == CellState.ALIVE) {
			//isolation A + x<2 -> D
			if (aliveNeighbours < 2) {
				nextState = CellState.DEAD;
			}
			//survive A + 2<x<3 -> A
			else if (aliveNeighbours == 2 || aliveNeighbours == 3) {
				nextState = CellState.ALIVE;
			}
			//over-population A + x>3 -> D
			else if (aliveNeighbours > 3) {
				nextState = CellState.DEAD;
			}
		}
		//if the current cell is dead
		else {
			//born D + x=3 -> A
			if (aliveNeighbours == 3) {
				nextState = CellState.ALIVE;
			}
			//stays dead unless if is triggered
			else {
				nextState = CellState.DEAD;
			}
		}	
		
		return nextState;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		int neighbourCount = 0;
		//check if the cell is positioned along the side
		for (int i = -1; i<2; i++) {
			for (int j = -1; j<2; j++) {
				if (!(i==0 && j==0)) {
					int rx = row + i;
					int ry = col + j;
					
					if (((rx >= 0) && (ry >= 0)) && ((rx < numberOfRows()) && (ry < numberOfColumns()))) {
						if (getCellState(rx,ry) == state) {
							neighbourCount ++;
						}	
					}
				}
			}
		}
		return neighbourCount;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
