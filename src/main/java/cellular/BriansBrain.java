package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{

	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}		
	
	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		CellState current = null;
		current = currentGeneration.get(row, col);
		return current;
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int r = 0; r < numberOfRows(); r++) {
			for (int c = 0; c < numberOfColumns(); c++) {
				CellState nextState = this.getNextCell(r, c);
				nextGeneration.set(r, c, nextState);	
			}	
		}
		currentGeneration = nextGeneration;
	}
	
	@Override
	public CellState getNextCell(int row, int col) {
		CellState nextState = null;
		CellState currentCS = getCellState(row,col);
		int aliveNeighbours = countNeighbors(row,col,CellState.ALIVE);
		//living cell -> dying
		if (currentCS == CellState.ALIVE) {
			nextState = CellState.DYING;
		}
		//dying cell -> dead
		else if (currentCS == CellState.DYING) {
			nextState = CellState.DEAD;
		}
		//dead cell -> if 2 living neighbour -> alive, else dead
		else {
			if (aliveNeighbours == 2) {
				nextState = CellState.ALIVE;
			}
			else {
				nextState = CellState.DEAD;
			}
		}
		return nextState;
	}

	private int countNeighbors(int row, int col, CellState state) {
		int neighbourCount = 0;
		//check if the cell is positioned along the side
		for (int i = -1; i<2; i++) {
			for (int j = -1; j<2; j++) {
				if (!(i==0 && j==0)) {
					int rx = row + i;
					int ry = col + j;
					
					if (((rx >= 0) && (ry >= 0)) && ((rx < numberOfRows()) && (ry < numberOfColumns()))) {
						if (getCellState(rx,ry) == state) {
							neighbourCount ++;
						}	
					}
				}
			}
		}
		return neighbourCount;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}	
}